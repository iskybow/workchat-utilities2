package main

import (
	"os"

	"gitlab.com/iskybow/workchat-utilities2/mmgotool/commands"
)

func main() {
	if err := commands.Run(os.Args[1:]); err != nil {
		os.Exit(1)
	}
}
