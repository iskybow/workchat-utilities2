package main

var coreLabels = pullRequest
var communityPlugins = MergeLabels(coreLabels, []Label{securityReview})
var pluginLabelsWithoutHW = MergeLabels(coreLabels, issue, docs, plugin)
var pluginLabels = MergeLabels(pluginLabelsWithoutHW, helpWanted)

var defaultMapping = map[string][]Label{
	"workchat-icebreaker-plugin":        communityPlugins,
	"workchat-plugin-agenda":            pluginLabels,
	"workchat-plugin-antivirus":         pluginLabels,
	"workchat-plugin-api":               pluginLabels,
	"workchat-plugin-autolink":          pluginLabels,
	"workchat-plugin-autotranslate":     pluginLabels,
	"workchat-plugin-aws-SNS":           pluginLabels,
	"workchat-plugin-canary":            pluginLabels,
	"workchat-plugin-channel-export":    pluginLabels,
	"workchat-plugin-cloud":             pluginLabels,
	"workchat-plugin-community":         pluginLabels,
	"workchat-plugin-confluence":        pluginLabels,
	"workchat-plugin-custom-attributes": pluginLabels,
	"workchat-plugin-demo-creator":      pluginLabels,
	"workchat-plugin-demo":              pluginLabels,
	"workchat-plugin-dice-roller":       communityPlugins,
	"workchat-plugin-digitalocean":      communityPlugins,
	"workchat-plugin-docup":             pluginLabels,
	"workchat-plugin-email-reply":       pluginLabels,
	"workchat-plugin-giphy-moussetc":    communityPlugins,
	"workchat-plugin-github":            pluginLabels,
	"workchat-plugin-gitlab":            pluginLabels,
	"workchat-plugin-gmail":             communityPlugins,
	"workchat-plugin-google-calendar":   pluginLabels,
	"workchat-plugin-incident-response": pluginLabels,
	"workchat-plugin-jenkins":           pluginLabels,
	"workchat-plugin-jira":              pluginLabels,
	"workchat-plugin-jitsi":             pluginLabels,
	"workchat-plugin-memes":             pluginLabels,
	"workchat-plugin-mscalendar":        pluginLabels,
	"workchat-plugin-msteams-meetings":  pluginLabels,
	"workchat-plugin-nop":               pluginLabels,
	"workchat-plugin-nps":               pluginLabels,
	"workchat-plugin-oembed":            pluginLabels,
	"workchat-plugin-profanity-filter":  pluginLabels,
	"workchat-plugin-recommend":         communityPlugins,
	"workchat-plugin-skype4business":    pluginLabels,
	"workchat-plugin-solar-lottery":     pluginLabels,
	"workchat-plugin-starter-template":  pluginLabels,
	"workchat-plugin-suggestions":       pluginLabels,
	"workchat-plugin-todo":              pluginLabels,
	"workchat-plugin-walltime":          pluginLabels,
	"workchat-plugin-webex":             pluginLabels,
	"workchat-plugin-webrtc-video":      communityPlugins,
	"workchat-plugin-welcomebot":        pluginLabels,
	"workchat-plugin-workflow-client":   pluginLabels,
	"workchat-plugin-workflow":          pluginLabelsWithoutHW,
	"workchat-plugin-zoom":              pluginLabels,
	"standup-raven":                     communityPlugins,
}

var securityReview = Label{
	"3: Security Review", "Review requested from Security Team", "1d76db",
}

// PR is the list of labels typically used on PRs. Use --
var pullRequest = []Label{
	{"1: PM Review", "Requires review by a product manager", "006b75"},
	{"1: UX Review", "Requires review by a UX Designer", "7cdfe2"},
	{"2: Dev Review", "Requires review by a core committer", "eb6420"},
	{"3: QA Review", "Requires review by a QA tester", "7cdfe2"},
	{"4: Reviews Complete", "All reviewers have approved the pull request", "0e8a16"},
	{"AutoMerge", "Used by Mattermod to merge PR automatically", "b74533"},
	{"Awaiting Submitter Action", "Blocked on the author", "b60205"},
	{"Do Not Merge/Awaiting PR", "Awaiting another pull request before merging (e.g. server changes)", "a32735"},
	{"Do Not Merge", "Should not be merged until this label is removed", "a32735"},
	{"Lifecycle/frozen", "", "d3e2f0"},
	{"Lifecycle/1:stale", "", "5319e7"},
	{"Lifecycle/2:inactive", "", "a34523"},
	{"Lifecycle/3:orphaned", "", "111111"},
	{"QA Review Done", "PR has been approved by QA", "0e8a16"},
	{"Work In Progress", "Not yet ready for review", "e11d21"},
}

var issue = []Label{
	{"Duplicate", "This issue or pull request already exists", "cfd3d7"},
	{"Invalid", "This doesn't seem right", "e4e669"},
	{"Triage", "", "efcb6e"},
	{"Type/Bug", "Something isn't working", "d73a4a"},
	{"Type/Enhancement", "New feature or improvement of existing feature", "a2eeef"},
	{"Type/Task", "A general task", "6698d1"},
	{"Type/Question", "Further information is requested", "d876e3"},
	{"Wontfix", "This will not be worked on", "ffffff"},
}

var plugin = []Label{
	{"Needs Workchat Changes", "Requires changes to the Workchat Plugin tookit", "9c14c9"},
	{"Setup Cloud Test Server", "Setup a test server using Workchat Cloud", "0052cc"},
	{"Setup HA Cloud Test Server", "Setup an HA test server using Workchat Cloud", "00efff"},
}

var helpWanted = []Label{
	{"Difficulty/1:Easy", "Easy ticket", "c2e0c6"},
	{"Difficulty/2:Medium", "Medium ticket", "bfdadc"},
	{"Difficulty/3:Hard", "Hard ticket", "f9d0c4"},
	{"Good First Issue", "Suitable for first-time contributors", "7057ff"},
	{"Hacktoberfest", "", "dc7d02"},
	{"Help Wanted", "Community help wanted", "33aa3f"},
	{"Needs Spec", "Needs further specification to be a good (help wanted) ticket", "e25de0"},
	{"Tech/Go", "", "0e8a16"},
	{"Tech/ReactJS", "", "1d76db"},
	{"Tech/TypeScript", "", "c9ffff"},
	{"Up For Grabs", "Ready for help from the community. Removed when someone volunteers", "8B4500"},
}

var docs = []Label{
	{"Docs/Done", "Required documentation has been written", "0e8a16"},
	{"Docs/Needed", "Requires documentation", "b60205"},
	{"Docs/Not Needed", "Does not require documentation", "d4c5f9"},
}

var migrateMap = map[string]string{
	"Bug":             "Type/Bug",
	"Enhancement":     "Type/Enhancement",
	"Question":        "Type/Question",
	"Tech/JavaScript": "",
	// Migrate QA labels:
	"2: QA Review":        "3: QA Review",
	"3: Reviews Complete": "4: Reviews Complete",
}
