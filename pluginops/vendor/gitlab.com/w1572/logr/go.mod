module gitlab.com/w1572/logr

go 1.17

require (
	github.com/francoispqt/gojay v1.2.13
	github.com/stretchr/testify v1.7.5
	github.com/wiggin77/merror v1.0.3
	github.com/wiggin77/srslog v1.0.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
