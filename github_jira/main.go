package main

import (
	"os"

	"gitlab.com/iskybow/workchat-utilities2/github_jira/cmd"
)

func main() {
	if err := cmd.Run(os.Args[1:]); err != nil {
		os.Exit(1)
	}
}
