package cmd

import (
	"github.com/spf13/cobra"
)

type Command = cobra.Command

func Run(args []string) error {
	RootCmd.SetArgs(args)
	return RootCmd.Execute()
}

var RootCmd = &cobra.Command{
	Use:   "github_jira",
	Short: "Manage Workchat Github & Jira",
	Long:  "Workchat CLI for managing & synchronizing Workchat github & jira",
}
