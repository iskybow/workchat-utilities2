module gitlab.com/iskybow/workchat-utilities2/github_jira

go 1.16

require (
	github.com/google/go-github/v35 v35.3.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.1.3
	golang.org/x/oauth2 v0.0.0-20210615190721-d04028783cf1
)
