package github

const templateContributing = `
----
If you're interested please comment here and come [join our "Contributors" community channel]  on our daily build server, where you can discuss questions with community members and the Workchat core team. For technical advice or questions, please  [join our "Developers" community channel]  .

New contributors please see our [Developer's Guide]   .

JIRA: 
`
